.DEFAULT_GOAL := run

run:
	cargo run

build:
	cargo build

test:
	cargo test
