use std::ops::{Add, Sub, Mul, Div};
use crate::linear_algebra::{Dot, Cross};
use crate::simplefloat::SimpleFloat;
use serde::Serialize;
use serde::Deserialize;

#[repr(C)]
#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Vec3<T>
{
    pub values: [T; 3]
}

impl<T> From<[T; 3]> for Vec3<T>
where T: Copy
{
    fn from(other: [T; 3]) -> Self
    {
        Self
        {
            values: other
        }
    }
}

impl<T> From<Vec3<SimpleFloat<T>>> for Vec3<f32>
where T: Into<f32> + Copy
{
    fn from(other: Vec3<SimpleFloat<T>>) -> Vec3<f32>
    {
        Self
        {
            values: [other.values[0].into(), other.values[1].into(), other.values[2].into()]
        }
    }
}

impl From<Vec3<i16>> for Vec3<f32>
{
    fn from(other: Vec3<i16>) -> Self
    {
        Self
        {
            values: [f32::from(other.values[0]), f32::from(other.values[1]), f32::from(other.values[2])]
        }
    }
}

impl<T> Into<[T; 3]> for &Vec3<T>
where T: Copy
{
    fn into(self) -> [T; 3]
    {
        self.values
    }
}

impl<T> Into<[T; 3]> for Vec3<T>
where T: Copy
{
    fn into(self) -> [T; 3]
    {
        self.values
    }
}

impl<T, U> Add<Vec3<U>> for Vec3<T>
where T: Copy + Add<Output=T> + From<U>,
      U: Copy
{
    type Output = Vec3<T>;

    fn add(self, other: Vec3<U>) -> Self::Output
    {
        &self + &other
    }
}

impl<T, U> Add<Vec3<U>> for &Vec3<T>
where T: Copy + Add<Output=T> + From<U>,
      U: Copy
{
    type Output = Vec3<T>;

    fn add(self, other: Vec3<U>) -> Self::Output
    {
        self + &other
    }
}

impl<T, U> Add<&Vec3<U>> for Vec3<T>
where T: Copy + Add<Output=T> + From<U>,
      U: Copy
{
    type Output = Vec3<T>;

    fn add(self, other: &Vec3<U>) -> Self::Output
    {
        &self + other
    }
}

impl<T> Sub<Vec3<T>> for Vec3<T>
where T: Copy + Sub<Output=T>
{
    type Output = Vec3<T>;

    fn sub(self, other: Vec3<T>) -> Self::Output
    {
        &self - &other
    }
}

impl<T> Sub<&Vec3<T>> for Vec3<T>
where T: Copy + Sub<Output=T>
{
    type Output = Vec3<T>;

    fn sub(self, other: &Vec3<T>) -> Self::Output
    {
        &self - other
    }
}

impl<T> Sub<Vec3<T>> for &Vec3<T>
where T: Copy + Sub<Output=T>
{
    type Output = Vec3<T>;

    fn sub(self, other: Vec3<T>) -> Self::Output
    {
        self - &other
    }
}

impl<'a, 'b, T, U> Add<&'a Vec3<U>> for &'b Vec3<T>
where T: Copy + Add<Output=T> + From<U>,
      U: Copy
{
    type Output = Vec3<T>;

    fn add(self, other: &'a Vec3<U>) -> Self::Output
    {
        Self::Output::from([
            self.values[0] + T::from(other.values[0]),
            self.values[1] + T::from(other.values[1]),
            self.values[2] + T::from(other.values[2])
        ])
    }
}

impl<'a, 'b, T> Sub<&'a Vec3<T>> for &'b Vec3<T>
where T: Copy + Sub<Output=T>
{
    type Output = Vec3<T>;

    fn sub(self, other: &'a Vec3<T>) -> Self::Output
    {
        Self::Output::from([
            self.values[0] - other.values[0],
            self.values[1] - other.values[1],
            self.values[2] - other.values[2]
        ])
    }
}

impl<T> Mul<T> for Vec3<T>
where T: Copy + Mul<Output=T>
{
    type Output = Vec3<T>;

    fn mul(self, scale: T) -> Self::Output {
        Self::Output::from([
            scale * self.values[0],
            scale * self.values[1],
            scale * self.values[2]
        ])
    }
}

impl<T> Dot<T> for Vec3<T>
where T: Copy + Add<Output=T> + Mul<Output=T>
{
    fn dot(&self, other: &Self) -> T
    {
        return (self.values[0] * other.values[0]) + (self.values[1] * other.values[1]) + (self.values[2] * other.values[2]);
    }
}

impl<T> Cross for Vec3<T>
where T: Copy + Add<Output=T> + Sub<Output=T> + Mul<Output=T>
{
    fn cross(&self, other: &Self) -> Self
    {
        Self::from([
            self.values[1] * other.values[2] - self.values[2] * other.values[1],
            self.values[2] * other.values[0] - self.values[0] * other.values[2],
            self.values[0] * other.values[1] - self.values[1] * other.values[0],
        ])
    }
}

impl<T> Vec3<T>
where T: Copy + Add<Output=T> + Sub<Output=T> + Mul<Output=T>
{
    pub fn from_raw(v0: T, v1: T, v2: T) -> Self
    {
        Self::from([v0, v1, v2])
    }
}

impl<T> Clone for Vec3<T>
where T: Clone
{
    fn clone(&self) -> Self
    {
        Self
        {
            values: self.values.clone()
        }
    }
}

impl<T> Copy for Vec3<T>
where T: Copy
{
}

impl<T> Vec3<T>
where T: Copy
{
    pub fn at(&self, index: usize) -> T
    {
        self.values[index]
    }

    pub fn as_ptr(&self) -> *const T
    {
        self.values.as_ptr()
    }

    pub fn into_raw(self) -> [T; 3]
    {
        self.values
    }
}

pub trait Sqrt
{
    fn sqrt(self) -> Self;
}

impl Sqrt for f32
{
    #[inline(always)]
    fn sqrt(self) -> Self
    {
        f32::sqrt(self)
    }
}

impl Sqrt for f64
{
    #[inline(always)]
    fn sqrt(self) -> Self
    {
        f64::sqrt(self)
    }
}

impl<T> Vec3<T>
where T: Sqrt + Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    pub fn length(&self) -> T
    {
        self.dot(self).sqrt()
    }

    pub fn normalize(&self) -> Self
    {
        let length = self.length();

        Self::from([self.values[0] / length, self.values[1] / length, self.values[2] / length])
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_from_raw()
    {
        assert_eq!(Vec3::from_raw(1,2,3).values, [1,2,3]);
        assert_eq!(Vec3::from_raw(4,2,3).values, [4,2,3]);
    }

    #[test]
    fn test_from_array()
    {
        assert_eq!(Vec3::<i32>::from([1,2,3]).values, [1,2,3]);
        assert_eq!(Vec3::<i32>::from([4,2,3]).values, [4,2,3]);
        assert_eq!(Vec3::<i16>::from([4,2,3]).values, [4,2,3]);
    }

    #[test]
    fn test_from_array_of_another_type()
    {
        assert_eq!(Vec3::<f32>::from(Vec3::<i16>::from([1,2,3])), Vec3::from([1.0, 2.0, 3.0]));
    }

    #[test]
    fn test_dot()
    {
        assert_eq!(Vec3::from_raw(1,0,0).dot(&Vec3::from_raw(1,0,0)), 1);
        assert_eq!(Vec3::from_raw(1,0,0).dot(&Vec3::from_raw(0,1,0)), 0);
    }

    #[test]
    fn test_eq()
    {
        assert_eq!(Vec3::from_raw(1,2,3), Vec3::from_raw(1,2,3));
        assert_eq!(Vec3::from_raw(1,3,3), Vec3::from_raw(1,3,3));
    }

    #[test]
    fn test_neq()
    {
        assert_ne!(Vec3::from_raw(1,2,3), Vec3::from_raw(1,5,3));
        assert_ne!(Vec3::from_raw(1,3,3), Vec3::from_raw(1,3,7));
    }

    #[test]
    fn test_cross()
    {
        assert_eq!(Vec3::<i32>::from([1,0,0]).cross(&Vec3::from([0,1,0])), Vec3::from([0,0,1]));
        assert_eq!(Vec3::<i32>::from([1,0,0]).cross(&Vec3::from([0,0,1])), Vec3::from([0,-1,0]));
    }

    #[test]
    fn test_add()
    {
        assert_eq!(Vec3::<i32>::from([1,0,0]) + Vec3::<i32>::from([0,1,0]), Vec3::from([1,1,0]));
        assert_eq!(Vec3::<i32>::from([1,0,1]) + Vec3::<i32>::from([0,1,1]), Vec3::from([1,1,2]));
    }

    #[test]
    fn test_add_one_ref()
    {
        assert_eq!(Vec3::<i32>::from([1,0,0]) + &Vec3::<i32>::from([0,1,0]), Vec3::from([1,1,0]));
        assert_eq!(&Vec3::<i32>::from([1,0,1]) + Vec3::<i32>::from([0,1,1]), Vec3::from([1,1,2]));
    }

    #[test]
    fn test_add_different_types()
    {
        assert_eq!(Vec3::<f64>::from([1.0,0.0,0.0]) + Vec3::<i32>::from([0,1,0]), Vec3::from([1.0,1.0,0.0]));
    }

    #[test]
    fn test_sub()
    {
        assert_eq!(Vec3::<i32>::from([1,0,0]) - Vec3::<i32>::from([0,1,0]), Vec3::from([1,-1,0]));
        assert_eq!(Vec3::<i32>::from([1,0,1]) - Vec3::<i32>::from([0,1,1]), Vec3::from([1,-1,0]));
    }

    #[test]
    fn test_sub_one_ref()
    {
        assert_eq!(Vec3::<i32>::from([1,0,0]) - &Vec3::<i32>::from([0,1,0]), Vec3::from([1,-1,0]));
        assert_eq!(&Vec3::<i32>::from([1,0,1]) - Vec3::<i32>::from([0,1,1]), Vec3::from([1,-1,0]));
    }
}
