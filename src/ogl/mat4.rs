use std::ops::{Add, Sub, Mul, Div, Neg};
use std::fmt::{Display, Debug, Formatter, Error};
use num_traits::{One, Zero};
use crate::linear_algebra::{Determinant, Transpose, Inverse};
use super::mat3::Mat3;
use super::vec4::Vec4;
use serde::Serialize;
use serde::Deserialize;

#[repr(C)]
#[derive(PartialEq, Copy, Clone, Serialize, Deserialize)]
pub struct Mat4<T>
{
    pub values: [T; 16]
}

impl<T> Into<[T; 16]> for &Mat4<T>
where T: Copy
{
    fn into(self) -> [T; 16]
    {
        self.values
    }
}

impl<T> Transpose for Mat4<T>
where T: Copy + Clone
{
    fn transpose(&self) -> Self
    {
        Mat4::from([
            self.at(0,0), self.at(1,0), self.at(2,0), self.at(3,0),
            self.at(0,1), self.at(1,1), self.at(2,1), self.at(3,1),
            self.at(0,2), self.at(1,2), self.at(2,2), self.at(3,2),
            self.at(0,3), self.at(1,3), self.at(2,3), self.at(3,3)
        ])
    }
}

impl<T> Mat4<T>
where T: Copy
{
    pub fn as_ptr(&self) -> *const T
    {
        self.values.as_ptr()
    }

    #[inline(always)]
    pub fn at(&self, i: usize, j: usize) -> T
    {
        self.values[4*i + j]
    }
}

impl<T> Mat4<T>
where T: Copy + Mul<Output=T>
{
    pub fn scale(self, a: T) -> Self
    {
        Self::from([
            a * self.at(0,0), a * self.at(0,1), a * self.at(0,2), a * self.at(0,3),
            a * self.at(1,0), a * self.at(1,1), a * self.at(1,2), a * self.at(1,3),
            a * self.at(2,0), a * self.at(2,1), a * self.at(2,2), a * self.at(2,3),
            a * self.at(3,0), a * self.at(3,1), a * self.at(3,2), a * self.at(3,3)
        ])
    }
}

impl<T> Mat4<T>
where T: Copy + One + Neg<Output=T> + Add<Output=T> + Sub<Output=T> + Mul<Output=T>
{
    #[inline(always)]
    pub fn minor(&self, i: usize, j: usize) -> T
    {
        #[inline(always)]
        fn index_set_from(x: usize) -> Option<[usize; 3]>
        {
            match x
            {
                0 => Some([1,2,3]),
                1 => Some([0,2,3]),
                2 => Some([0,1,3]),
                3 => Some([0,1,2]),
                _ => None
            }
        }

        let i_set : [usize; 3] = index_set_from(i).unwrap();
        let j_set : [usize; 3] = index_set_from(j).unwrap();

        Mat3::<T>::from([
            self.at(i_set[0], j_set[0]), self.at(i_set[0], j_set[1]), self.at(i_set[0], j_set[2]),
            self.at(i_set[1], j_set[0]), self.at(i_set[1], j_set[1]), self.at(i_set[1], j_set[2]),
            self.at(i_set[2], j_set[0]), self.at(i_set[2], j_set[1]), self.at(i_set[2], j_set[2]),
        ]).det()
    }

    pub fn cofactor(&self, i: usize, j: usize) -> T
    {
        let sign = if (i + j) % 2 == 0 { T::one() } else { - T::one() };

        sign * self.minor(i,j)
    }
}

impl<T> Mat4<T>
where T: Copy + std::marker::Sized + Zero + One + Neg<Output=T> + Add<Output=T> + Sub<Output=T> + Mul<Output=T> + Div<Output=T> + PartialEq
{
    pub fn inverse_transpose(&self) -> Option<Self>
    {
        let det = self.det();

        if det == T::zero()
        {
            None
        }
        else
        {
            Some(Self::from([
                self.cofactor(0,0),self.cofactor(0,1),self.cofactor(0,2),self.cofactor(0,3),
                self.cofactor(1,0),self.cofactor(1,1),self.cofactor(1,2),self.cofactor(1,3),
                self.cofactor(2,0),self.cofactor(2,1),self.cofactor(2,2),self.cofactor(2,3),
                self.cofactor(3,0),self.cofactor(3,1),self.cofactor(3,2),self.cofactor(3,3)
            ]).scale(T::one()/det))
        }
    }
}

impl<T> Determinant<T> for Mat4<T>
where T: Copy + One + Neg<Output=T> + Add<Output=T> + Sub<Output=T> + Mul<Output=T>
{
    fn det(&self) -> T
    {
        self.at(0,0) * self.cofactor(0,0)
            + self.at(0,1) * self.cofactor(0,1)
            + self.at(0,2) * self.cofactor(0,2)
            + self.at(0,3) * self.cofactor(0,3)
    }
}

impl<T> Inverse for Mat4<T>
where T: Copy + std::marker::Sized + Zero + One + Neg<Output=T> + Add<Output=T> + Sub<Output=T> + Mul<Output=T> + Div<Output=T> + PartialEq
{
    fn inverse(&self) -> Option<Self>
    {
        let inverse_transpose = self.inverse_transpose();

        match inverse_transpose
        {
            Some(a) => Some(a.transpose()),
            None => None
        }
    }
}

impl<T> Debug for Mat4<T>
where T: Display + Copy
{
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error>
    {
        write!(f, "Mat4 {{
[ {}, {}, {}, {} ]
[ {}, {}, {}, {} ]
[ {}, {}, {}, {} ]
[ {}, {}, {}, {} ]
}}",
               self.at(0,0), self.at(0,1), self.at(0,2), self.at(0,3),
               self.at(1,0), self.at(1,1), self.at(1,2), self.at(1,3),
               self.at(2,0), self.at(2,1), self.at(2,2), self.at(2,3),
               self.at(3,0), self.at(3,1), self.at(3,2), self.at(3,3)
        )
    }
}

impl<T> From<Mat3<T>> for Mat4<T>
where T: Copy + Zero + One
{
    fn from(mat: Mat3<T>) -> Self
    {
        Self::from([
            mat.at(0,0), mat.at(0,1), mat.at(0,2), T::zero(),
            mat.at(1,0), mat.at(1,1), mat.at(1,2), T::zero(),
            mat.at(2,0), mat.at(2,1), mat.at(2,2), T::zero(),
            T::zero(), T::zero(), T::zero(), T::one()
        ])
    }
}

impl<T> From<[T; 16]> for Mat4<T>
where T: Copy
{
    fn from(values: [T; 16]) -> Self
    {
        Self
        {
            values: values
        }
    }
}


impl<T> From<[[T; 4]; 4]> for Mat4<T>
where T: Copy + Default
{
    fn from(rows: [[T; 4]; 4]) -> Self
    {
        let mut new_values = [T::default(); 16];

        for i in 0..4
        {
            for j in 0..4
            {
                new_values[i*4 + j] = rows[j][i];
            }
        }

        Self
        {
            values: new_values
        }
    }
}


impl<'a, 'b, T> Mul<&'b Mat4<T>> for &'a Mat4<T>
where T: Copy + Add<Output = T> + Mul<Output = T> + Default
{
    type Output = Mat4<T>;

    fn mul(self, other: &'b Mat4<T>) -> Self::Output
    {
        let mut new_values = [T::default(); 16];

        for i in 0..4
        {
            for j in 0..4
            {
                for k in 0..4
                {
                    let index = i*4 + j;
                    new_values[index] = new_values[index] + (self.values[k*4 + j] * other.values[i*4 + k]);
                }
            }
        }

        Self::Output
        {
            values: new_values
        }
    }
}

impl<T> Mul for Mat4<T>
where T: Copy + Add<Output = T> + Mul<Output = T> + Default
{
    type Output = Mat4<T>;

    fn mul(self, other: Self) -> Self
    {
        &self * &other
    }
}

impl<'a, T> Mul<Vec4<T>> for &'a Mat4<T>
where T: Copy + Add<Output = T> + Mul<Output = T> + Default
{
    type Output = Vec4<T>;

    #[inline(always)]
    fn mul(self, other: Vec4<T>) -> Self::Output
    {
        let mut new_values = [T::default(); 4];

        for i in 0..4
        {
            for j in 0..4
            {
                let index = i;

                new_values[index] = new_values[index] + (self.values[j*4 + i] * other.values[j]);
            }
        }

        Self::Output
        {
            values: new_values
        }
    }
}


impl<T> Mul<Vec4<T>> for Mat4<T>
where T: Copy + Add<Output = T> + Mul<Output = T> + Default
{
    type Output = Vec4<T>;

    #[inline(always)]
    fn mul(self, other: Vec4<T>) -> Self::Output
    {
        &self * other
    }
}


#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_from_array()
    {
        assert_eq!(Mat4::from([1, 0, 0, 0,
                               0, 1, 0, 0,
                               0, 0, 1, 0,
                               0, 0, 0, 1]).values,
                   [1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1]);
        assert_eq!(Mat4::from([1, 2, 0, 0,
                               0, 1, 2, 0,
                               0, 0, 1, 2,
                               0, 0, 0, 1]).values,
                   [1, 2, 0, 0,
                    0, 1, 2, 0,
                    0, 0, 1, 2,
                    0, 0, 0, 1]);
    }

    #[test]
    fn test_from_rows()
    {
        assert_eq!(Mat4::from([[1, 0, 0, 0],
                               [0, 1, 0, 0],
                               [0, 0, 1, 0],
                               [0, 0, 0, 1]]),
                   Mat4::from([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]));

        assert_eq!(Mat4::from([[1, 1, 0, 0],
                               [0, 1, 0, 0],
                               [0, 0, 1, 0],
                               [0, 0, 0, 1]]),
                   Mat4::from([1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]));

        assert_eq!(Mat4::from([[1, 0, 0, 0],
                               [1, 1, 0, 0],
                               [0, 0, 1, 0],
                               [0, 0, 0, 1]]),
                   Mat4::from([1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]));
    }

    #[test]
    fn test_equality()
    {
        assert_eq!(Mat4::from([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]),
                   Mat4::from([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]));
        assert_eq!(Mat4::from([1, 0, 5, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]),
                   Mat4::from([1, 0, 5, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]));
    }

    #[test]
    fn test_mul_with_mat4()
    {
        let a = Mat4::from([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
        let b = Mat4::from([1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);

        assert_eq!(a * b, Mat4::from([1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]));
    }

    #[test]
    fn test_mul_with_vec4()
    {
        let a = Mat4::from([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
        let v = Vec4::from([1, 2, 3, 4]);

        assert_eq!(a * v, Vec4::from([1, 2, 3, 4]));
    }

    #[test]
    fn test_ref_mul_with_vec4()
    {
        let a = Mat4::from([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]);
        let v = Vec4::from([1, 2, 3, 4]);

        assert_eq!(&a * v, Vec4::from([1, 2, 3, 4]));
    }

    #[test]
    fn test_transpose()
    {
        let mat: Mat4<i8> = Mat4::from([1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1]);

        assert_eq!(mat.transpose(), Mat4::from([1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1]));
        assert_eq!(mat.transpose().transpose(), mat);
    }

    #[test]
    fn test_transpose2()
    {
        let mat: Mat4<i8> = Mat4::from([
            -1,0,-1,1,
            0,0,1,-1,
            0,1,0,-1,
            0,-1,-1,1
        ]);

        assert_eq!(mat.transpose(), Mat4::<i8>::from([
            -1,0,0,0,
            0,0,1,-1,
            -1,1,0,-1,
            1,-1,-1,1
        ]));
    }

    #[test]
    fn test_from_mat3()
    {
        let mat: Mat4<i8> = Mat4::from(Mat3::from([
            1, 2, 3,
            4, 5, 6,
            7, 8, 9
        ]));

        assert_eq!(mat, Mat4::from([
            1, 2, 3, 0,
            4, 5, 6, 0,
            7, 8, 9, 0,
            0, 0, 0, 1
        ]));
    }

    #[test]
    fn test_minor()
    {
        let mat: Mat4<i8> = Mat4::from([1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,1]);

        assert_eq!(mat.minor(0,0), -1);
        assert_eq!(mat.minor(1,0), 1);
        assert_eq!(mat.minor(2,0), -1);
        assert_eq!(mat.minor(3,0), -2);
        assert_eq!(mat.minor(0,1), 1);
        assert_eq!(mat.minor(1,1), -1);
        assert_eq!(mat.minor(2,1), -2);
        assert_eq!(mat.minor(3,1), -1);
        assert_eq!(mat.minor(0,2), -1);
        assert_eq!(mat.minor(1,2), -2);
        assert_eq!(mat.minor(2,2), -1);
        assert_eq!(mat.minor(3,2), 1);
        assert_eq!(mat.minor(0,3), -2);
        assert_eq!(mat.minor(1,3), -1);
        assert_eq!(mat.minor(2,3), 1);
        assert_eq!(mat.minor(3,3), -1);
    }

    #[test]
    fn test_cofactor()
    {
        let mat: Mat4<i8> = Mat4::from([1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,1]);

        assert_eq!(mat.cofactor(0,0), -1);
        assert_eq!(mat.cofactor(1,0), -1);
        assert_eq!(mat.cofactor(2,0), -1);
        assert_eq!(mat.cofactor(3,0), 2);
        assert_eq!(mat.cofactor(0,1), -1);
        assert_eq!(mat.cofactor(1,1), -1);
        assert_eq!(mat.cofactor(2,1), 2);
        assert_eq!(mat.cofactor(3,1), -1);
        assert_eq!(mat.cofactor(0,2), -1);
        assert_eq!(mat.cofactor(1,2), 2);
        assert_eq!(mat.cofactor(2,2), -1);
        assert_eq!(mat.cofactor(3,2), -1);
        assert_eq!(mat.cofactor(0,3), 2);
        assert_eq!(mat.cofactor(1,3), -1);
        assert_eq!(mat.cofactor(2,3), -1);
        assert_eq!(mat.cofactor(3,3), -1);
    }

    #[test]
    fn test_cofactor2()
    {
        let mat: Mat4<i8> = Mat4::from([
            1,0,0,0,
            1,1,0,1,
            0,0,1,1,
            0,1,1,1
        ]);

        assert_eq!(mat.cofactor(0,0), -1);
        assert_eq!(mat.cofactor(1,0), 0);
        assert_eq!(mat.cofactor(2,0), 0);
        assert_eq!(mat.cofactor(3,0), 0);

        assert_eq!(mat.cofactor(0,1), 0);
        assert_eq!(mat.cofactor(1,1), 0);
        assert_eq!(mat.cofactor(2,1), 1);
        assert_eq!(mat.cofactor(3,1), -1);

        assert_eq!(mat.cofactor(0,2), -1);
        assert_eq!(mat.cofactor(1,2), 1);
        assert_eq!(mat.cofactor(2,2), 0);
        assert_eq!(mat.cofactor(3,2), -1);

        assert_eq!(mat.cofactor(0,3), 1);
        assert_eq!(mat.cofactor(1,3), -1);
        assert_eq!(mat.cofactor(2,3), -1);
        assert_eq!(mat.cofactor(3,3), 1);
    }

    #[test]
    fn test_determinant()
    {
        assert_eq!(Mat4::<i8>::from([1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,1]).det(), -3);
        assert_eq!(Mat4::<i8>::from([1,1,1,0,1,1,0,1,1,0,1,2,0,1,1,1]).det(), -4);
    }

    #[test]
    fn test_inverse()
    {
        assert_eq!(
            Mat4::<i8>::from([1,0,0,0,1,1,0,1,0,0,1,1,0,1,1,1]).inverse(),
            Some(Mat4::<i8>::from([1,0,0,0,0,0,-1,1,1,-1,0,1,-1,1,1,-1]))
        )
    }
}
