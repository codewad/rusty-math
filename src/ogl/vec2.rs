//use std::ops::{Add, Sub, Mul, Div};
//use crate::linear_algebra::Dot;
//use crate::simplefloat::SimpleFloat;
use serde::Serialize;
use serde::Deserialize;

type InternalType<T> = [T; 2];

#[repr(C)]
#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize, Copy, Clone)]
pub struct Vec2<T>
{
    pub values: InternalType<T>
}

impl<T> From<InternalType<T>> for Vec2<T>
where T: Copy
{
    fn from(other: InternalType<T>) -> Self
    {
        Self
        {
            values: other
        }
    }
}

impl<T> Into<InternalType<T>> for &Vec2<T>
where T: Copy
{
    fn into(self) -> InternalType<T>
    {
        self.values
    }
}

impl<T> Into<InternalType<T>> for Vec2<T>
where T: Copy
{
    fn into(self) -> InternalType<T>
    {
        self.values
    }
}
