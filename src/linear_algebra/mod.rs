pub trait Dot<T>
{
    fn dot(&self, other: &Self) -> T;
}

pub trait Cross
{
    fn cross(&self, other: &Self) -> Self;
}

pub trait Transpose
{
    fn transpose(&self) -> Self;
}

pub trait Determinant<T>
{
    fn det(&self) -> T;
}

pub trait Inverse
where Self: std::marker::Sized
{
    fn inverse(&self) -> Option<Self>;
}
