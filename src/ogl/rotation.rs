use super::{Mat3, Vec3};

pub fn rotatey_matrix(angle: f32) -> Mat3<f32>
{
    let cosine = angle.cos();
    let sine = angle.sin();

    Mat3::<f32>::from([
        cosine, 0.0, -sine,
        0.0, 1.0, 0.0,
        sine, 0.0, cosine
    ])
}

pub fn rotatex_matrix(angle: f32) -> Mat3<f32>
{
    let cosine = angle.cos();
    let sine = angle.sin();

    Mat3::<f32>::from([
        1.0, 0.0, 0.0,
        0.0, cosine, -sine,
        0.0, sine, cosine
    ])
}

pub fn rotatez_matrix(angle: f32) -> Mat3<f32>
{
    let cosine = angle.cos();
    let sine = angle.sin();

    Mat3::<f32>::from([
        cosine, -sine, 0.0,
        sine, cosine, 0.0,
        0.0, 0.0, 1.0,
    ])
}

struct Calculation {
    cosine: f32,
    sine: f32,
    diff_cosine: f32,
    axis: Vec3<f32>
}

impl Calculation
{
    fn new(angle: f32, axis: Vec3<f32>) -> Self
    {
        let cosine = angle.cos();

        Self
        {
            cosine : cosine,
            diff_cosine : 1.0 - cosine,
            sine : angle.sin(),
            axis : axis
        }
    }

    #[inline(always)]
    fn get_main(&self, i: usize) -> f32
    {
        let v = self.axis.at(i);

        self.cosine + v * v * self.diff_cosine
    }

    #[inline(always)]
    fn get_off_main(&self, i: usize, j: usize) -> f32
    {
        #[inline(always)]
        fn other_index_from(x: usize, y: usize) -> Option<usize>
        {
            match (x, y)
            {
                (0, 1) => Some(2),
                (1, 0) => Some(2),
                (0, 2) => Some(1),
                (2, 0) => Some(1),
                (1, 2) => Some(0),
                (2, 1) => Some(0),
                _ => None
            }
        }

        #[inline(always)]
        fn get_sign(x: usize, y: usize) -> Option<f32>
        {
            match (x, y)
            {
                (0, 1) => Some(-1.0),
                (0, 2) => Some(1.0),
                (1, 0) => Some(1.0),
                (1, 2) => Some(-1.0),
                (2, 0) => Some(-1.0),
                (2, 1) => Some(1.0),
                _ => None
            }
        }

        let o = other_index_from(i, j).unwrap();
        let sign = get_sign(i, j).unwrap();

        self.axis.at(i) * self.axis.at(j) * self.diff_cosine
            + sign * self.axis.at(o) * self.sine
    }
}

pub fn rotate_around_vector(axis: Vec3<f32>, angle: f32) -> Mat3<f32>
{
    let calc = Calculation::new(angle, axis);

    Mat3::<f32>::from([
        calc.get_main(0), calc.get_off_main(0, 1), calc.get_off_main(0, 2),
        calc.get_off_main(1, 0), calc.get_main(1), calc.get_off_main(1, 2),
        calc.get_off_main(2, 0), calc.get_off_main(2, 1), calc.get_main(2)
    ])
}

#[cfg(test)]
mod tests
{
    use super::rotate_around_vector;
    use super::Vec3;

    fn assert_threshold(actual: Vec3<f32>, expected: Vec3<f32>, threshold: Vec3<f32>)
    {
        let assert_value_diff = |index: usize| {
            let diff = (actual.at(index) - expected.at(index)).abs();
            assert!(diff < threshold.at(index), "failure matching index: {:?} ({:?}, {:?}, {:?})", index, actual.at(index), expected.at(index), threshold.at(index));
        };

        assert_value_diff(0);
        assert_value_diff(1);
        assert_value_diff(2);
    }

    #[test]
    fn test_rotate_1()
    {
        let rotation = rotate_around_vector(Vec3::<f32>::from([0.0, 1.0, 0.0]), std::f32::consts::PI / 2.0);
        println!("{:?}", rotation);

        assert_threshold(
            &rotation * Vec3::<f32>::from([0.0, 1.0, 0.0]),
            Vec3::<f32>::from([0.0, 1.0, 0.0]),
            Vec3::<f32>::from([0.0001, 0.0001, 0.0001]),
        );

        assert_threshold(
            &rotation * Vec3::<f32>::from([1.0, 0.0, 0.0]),
            Vec3::<f32>::from([0.0, 0.0, -1.0]),
            Vec3::<f32>::from([0.0001, 0.0001, 0.0001]),
        );
        assert_threshold(
            &rotation * Vec3::<f32>::from([0.0, 0.0, -1.0]),
            Vec3::<f32>::from([-1.0, 0.0, 0.0]),
            Vec3::<f32>::from([0.0001, 0.0001, 0.0001]),
        );

        assert_threshold(
            &rotation * Vec3::<f32>::from([-1.0, 0.0, 0.0]),
            Vec3::<f32>::from([0.0, 0.0, 1.0]),
            Vec3::<f32>::from([0.0001, 0.0001, 0.0001]),
        );
        assert_threshold(
            &rotation * Vec3::<f32>::from([0.0, 0.0, 1.0]),
            Vec3::<f32>::from([1.0, 0.0, 0.0]),
            Vec3::<f32>::from([0.0001, 0.0001, 0.0001]),
        );
    }
}
