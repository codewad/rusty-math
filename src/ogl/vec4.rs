use std::ops::{Div, Mul, Add};

#[repr(C)]
#[derive(Debug, PartialEq)]
pub struct Vec4<T>
{
    pub values: [T; 4]
}


pub trait Sqrt
{
    fn sqrt(self) -> Self;
}


impl Sqrt for f32
{
    #[inline(always)]
    fn sqrt(self) -> Self
    {
        self.sqrt()
    }
}


impl<T> Vec4<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T> + From<f32> + Sqrt
{
    pub fn length(&self) -> T
    {
        (self.values[0]*self.values[0] +
         self.values[1]*self.values[1] +
         self.values[2]*self.values[2] +
         self.values[3]*self.values[3]).sqrt()
    }

    pub fn normalize(&self) -> Self
    {
        let length = self.length();

        Self::from([self.values[0]/length,
                    self.values[1]/length,
                    self.values[2]/length,
                    self.values[3]/length])
    }
}

impl<T> From<[T; 4]> for Vec4<T>
{
    fn from(values: [T; 4]) -> Self
    {
        Self
        {
            values: values
        }
    }
}


#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_from_array()
    {
        assert_eq!(Vec4::from([1,2,3,4]).values, [1,2,3,4]);
    }

    #[test]
    fn test_normalize()
    {
        assert_eq!(Vec4::from([5.0, 0.0, 0.0, 0.0]).normalize(), Vec4::from([1.0, 0.0, 0.0, 0.0]));
    }

    #[test]
    fn test_length()
    {
        assert_eq!(Vec4::from([1.0, 0.0, 0.0, 0.0]).length(), 1.0);
        assert_eq!(Vec4::from([0.0, 2.0, 0.0, 0.0]).length(), 2.0);
        assert_eq!(Vec4::from([0.0, 0.0, 3.0, 0.0]).length(), 3.0);
        assert_eq!(Vec4::from([0.0, 0.0, 0.0, 4.0]).length(), 4.0);
    }
}
