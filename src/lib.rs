mod checked_math;
mod simplefloat;
mod matrices;

pub mod ogl;
pub mod linear_algebra;

pub use simplefloat::SimpleFloat;
pub use matrices::Matrix;
