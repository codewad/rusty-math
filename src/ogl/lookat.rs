use std::ops::{Add, Sub, Mul, Div, Neg};
use super::{Mat4, Vec3};
use super::vec3::{Sqrt};
use crate::linear_algebra::{Cross, Dot};
use num_traits::{One, Zero};

pub fn look_at<T>(location: &Vec3<T>, target: &Vec3<T>, unoriented_up: &Vec3<T>) -> Mat4<T>
where T: Zero + One + Copy + Sqrt + Add<Output=T> + Sub<Output=T> + Mul<Output=T> + Div<Output=T> + Neg<Output=T>
{
    let z_axis: Vec3<T> = (location - target).normalize();
    let x_axis: Vec3<T> = z_axis.cross(unoriented_up).normalize();
    let y_axis: Vec3<T> = x_axis.cross(&z_axis);

    let z_values : [T; 3] = (&z_axis).into();
    let y_values : [T; 3] = (&y_axis).into();
    let x_values : [T; 3] = (&x_axis).into();

    Mat4::from([ x_values[0],  y_values[0],  z_values[0], T::zero(),
                 x_values[1],  y_values[1],  z_values[1], T::zero(),
                 x_values[2],  y_values[2],  z_values[2], T::zero(),
               -location.dot(&x_axis),-location.dot(&y_axis),-location.dot(&z_axis),  T::one()])
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_simple()
    {
        let location = Vec3::from([0.0, 0.0, 0.0]);
        let target = Vec3::from([0.0, 0.0, -5.0]);
        let up = Vec3::from([0.0, 1.0, 0.0]);
        let expected = Mat4::from([-1.0, 0.0, 0.0, 0.0,
                                    0.0, 1.0, 0.0, 0.0,
                                    0.0, 0.0, 1.0, 0.0,
                                    0.0, 0.0, 0.0, 1.0]);
        assert_eq!(look_at(&location, &target, &up), expected);
    }
}
