use std::ops::{Add, Sub, Mul, Div, Neg};
use std::fmt::{Display, Debug, Formatter, Error};
use crate::linear_algebra::{Transpose, Determinant, Inverse};
use super::Vec3;
use num_traits::{One, Zero};

#[repr(C)]
#[derive(PartialEq)]
pub struct Mat3<T>
{
    values: [T; 9]
}

impl<T> Into<[T; 9]> for &Mat3<T>
where T: Copy
{
    fn into(self) -> [T; 9]
    {
        self.values
    }
}

impl<T> Mat3<T>
where T: Copy
{
    #[inline(always)]
    pub fn at(&self, i: usize, j: usize) -> T
    {
        self.values[3*i + j]
    }
}

impl<T> Mat3<T>
where T: Copy + One + Neg<Output=T> + Sub<Output=T> + Mul<Output=T>
{
    #[inline(always)]
    pub fn minor(&self, i: usize, j: usize) -> T
    {
        #[inline(always)]
        fn index_pair_from(x: usize) -> Option<[usize; 2]>
        {
            match x
            {
                0 => Some([1,2]),
                1 => Some([0,2]),
                2 => Some([0,1]),
                _ => None
            }
        }

        let i_pair : [usize; 2] = index_pair_from(i).unwrap();
        let j_pair : [usize; 2] = index_pair_from(j).unwrap();

        self.at(i_pair[0], j_pair[0]) * self.at(i_pair[1], j_pair[1])
            - self.at(i_pair[1], j_pair[0]) * self.at(i_pair[0], j_pair[1])
    }

    #[inline(always)]
    pub fn cofactor(&self, i: usize, j: usize) -> T
    {
        let sign = if (i + j) % 2 == 0 { T::one() } else { - T::one() };

        sign * self.minor(i,j)
    }
}

impl<T> Mat3<T>
where T: Copy + Mul<Output=T>
{
    fn scale(self, a: T) -> Self
    {
        Self::from([
            a * self.at(0,0), a * self.at(0,1), a * self.at(0,2),
            a * self.at(1,0), a * self.at(1,1), a * self.at(1,2),
            a * self.at(2,0), a * self.at(2,1), a * self.at(2,2)
        ])
    }
}

impl<T> Transpose for Mat3<T>
where T: Copy
{
    fn transpose(&self) -> Self
    {
        Self::from([
            self.at(0,0), self.at(1,0), self.at(2,0),
            self.at(0,1), self.at(1,1), self.at(2,1),
            self.at(0,2), self.at(1,2), self.at(2,2)
        ])
    }
}

impl<T> Determinant<T> for Mat3<T>
where T: Copy + One + Neg<Output=T> + Add<Output=T> + Sub<Output=T> + Mul<Output=T>
{
    fn det(&self) -> T
    {
        self.at(0,0) * self.cofactor(0,0)
            + self.at(0,1) * self.cofactor(0,1)
            + self.at(0,2) * self.cofactor(0,2)
    }
}

impl<T> Inverse for Mat3<T>
where T: Copy + std::marker::Sized + Zero + One + Neg<Output=T> + Add<Output=T> + Sub<Output=T> + Mul<Output=T> + Div<Output=T> + PartialEq
{
    fn inverse(&self) -> Option<Self>
    {
        let det = self.det();

        if det == T::zero()
        {
            None
        }
        else
        {
            Some(Self::from([
                self.cofactor(0,0),self.cofactor(0,1),self.cofactor(0,2),
                self.cofactor(1,0),self.cofactor(1,1),self.cofactor(1,2),
                self.cofactor(2,0),self.cofactor(2,1),self.cofactor(2,2)
            ]).scale(T::one()/det).transpose())
        }
    }
}

impl<T> Debug for Mat3<T>
where T: Display + Copy
{
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error>
    {
        write!(f, "Mat3 {{
[ {}, {}, {} ]
[ {}, {}, {} ]
[ {}, {}, {} ]
}}",
               self.at(0,0), self.at(0,1), self.at(0,2),
               self.at(1,0), self.at(1,1), self.at(1,2),
               self.at(2,0), self.at(2,1), self.at(2,2)
        )
    }
}

impl<T> From<[T; 9]> for Mat3<T>
where T: Copy
{
    fn from(values: [T; 9]) -> Self
    {
        Self
        {
            values: values
        }
    }
}

impl<'a, 'b, T> Mul<&'b Vec3<T>> for &'a Mat3<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    type Output = Vec3<T>;

    #[inline(always)]
    fn mul(self, other: &'b Vec3<T>) -> Self::Output
    {
        let values : [T; 3] = other.into();

        Self::Output::from([
            self.values[0*3 + 0] * values[0] + self.values[0*3 + 1] * values[1] + self.values[0*3 + 2] * values[2],
            self.values[1*3 + 0] * values[0] + self.values[1*3 + 1] * values[1] + self.values[1*3 + 2] * values[2],
            self.values[2*3 + 0] * values[0] + self.values[2*3 + 1] * values[1] + self.values[2*3 + 2] * values[2]
        ])
    }
}

impl<'a, 'b, T> Mul<&'b Mat3<T>> for &'a Mat3<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    type Output = Mat3<T>;

    #[inline(always)]
    fn mul(self, other: &'b Mat3<T>) -> Self::Output
    {
        Self::Output::from([
            self.values[0*3+0] * other.values[0*3+0] + self.values[0*3+1] * other.values[1*3+0] + self.values[0*3+2] * other.values[2*3+0],
            self.values[0*3+0] * other.values[0*3+1] + self.values[0*3+1] * other.values[1*3+1] + self.values[0*3+2] * other.values[2*3+1],
            self.values[0*3+0] * other.values[0*3+2] + self.values[0*3+1] * other.values[1*3+2] + self.values[0*3+2] * other.values[2*3+2],

            self.values[1*3+0] * other.values[0*3+0] + self.values[1*3+1] * other.values[1*3+0] + self.values[1*3+2] * other.values[2*3+0],
            self.values[1*3+0] * other.values[0*3+1] + self.values[1*3+1] * other.values[1*3+1] + self.values[1*3+2] * other.values[2*3+1],
            self.values[1*3+0] * other.values[0*3+2] + self.values[1*3+1] * other.values[1*3+2] + self.values[1*3+2] * other.values[2*3+2],

            self.values[2*3+0] * other.values[0*3+0] + self.values[2*3+1] * other.values[1*3+0] + self.values[2*3+2] * other.values[2*3+0],
            self.values[2*3+0] * other.values[0*3+1] + self.values[2*3+1] * other.values[1*3+1] + self.values[2*3+2] * other.values[2*3+1],
            self.values[2*3+0] * other.values[0*3+2] + self.values[2*3+1] * other.values[1*3+2] + self.values[2*3+2] * other.values[2*3+2],
        ])
    }
}

impl<'a, T> Mul<Mat3<T>> for &'a Mat3<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    type Output = Mat3<T>;

    #[inline(always)]
    fn mul(self, other: Mat3<T>) -> Self::Output
    {
        self * &other
    }
}

impl<'a, T> Mul<&'a Mat3<T>> for Mat3<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    type Output = Mat3<T>;

    #[inline(always)]
    fn mul(self, other: &'a Mat3<T>) -> Self::Output
    {
        &self * other
    }
}

impl<T> Mul<Mat3<T>> for Mat3<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    type Output = Mat3<T>;

    #[inline(always)]
    fn mul(self, other: Mat3<T>) -> Self::Output
    {
        &self * &other
    }
}

impl<'a, T> Mul<Vec3<T>> for &'a Mat3<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    type Output = Vec3<T>;

    #[inline(always)]
    fn mul(self, other: Vec3<T>) -> Self::Output
    {
        self * &other
    }
}


impl<'a, T> Mul<&'a Vec3<T>> for Mat3<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    type Output = Vec3<T>;

    #[inline(always)]
    fn mul(self, other: &'a Vec3<T>) -> Self::Output
    {
        &self * other
    }
}


impl<T> Mul<Vec3<T>> for Mat3<T>
where T: Copy + Add<Output=T> + Mul<Output=T> + Div<Output=T>
{
    type Output = Vec3<T>;

    #[inline(always)]
    fn mul(self, other: Vec3<T>) -> Self::Output
    {
        &self * &other
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_eq()
    {
        assert_eq!(Mat3::<i8>::from([0,1,2,3,4,5,6,7,8]), Mat3::<i8>::from([0,1,2,3,4,5,6,7,8]));
    }

    #[test]
    fn test_mul_with_identity()
    {
        let identity: Mat3<i8> = Mat3::from([1,0,0,0,1,0,0,0,1]);

        assert_eq!(&identity * Mat3::<i8>::from([0,1,2,3,4,5,6,7,8]), Mat3::<i8>::from([0,1,2,3,4,5,6,7,8]));
        assert_eq!(Mat3::<i8>::from([0,1,2,3,4,5,6,7,8]) * &identity, Mat3::<i8>::from([0,1,2,3,4,5,6,7,8]));
        assert_eq!(Mat3::<i8>::from([0,1,2,3,4,5,6,7,8]) * identity, Mat3::<i8>::from([0,1,2,3,4,5,6,7,8]));
    }

    #[test]
    fn test_transpose()
    {
        let mat: Mat3<i8> = Mat3::from([1,1,1,0,0,0,1,1,1]);

        assert_eq!(mat.transpose(), Mat3::from([1,0,1,1,0,1,1,0,1]));
        assert_eq!(mat.transpose().transpose(), mat);
    }

    #[test]
    fn test_indexes()
    {
        let mat : Mat3<i8> = Mat3::from([1,2,3,
                                         4,5,6,
                                         7,8,9]);

        assert_eq!(mat.at(0,0), 1);
        assert_eq!(mat.at(1,0), 4);
        assert_eq!(mat.at(2,0), 7);
        assert_eq!(mat.at(0,1), 2);
        assert_eq!(mat.at(1,1), 5);
        assert_eq!(mat.at(2,1), 8);
        assert_eq!(mat.at(0,2), 3);
        assert_eq!(mat.at(1,2), 6);
        assert_eq!(mat.at(2,2), 9);
    }

    #[test]
    fn test_minor()
    {
        let mat : Mat3<i8> = Mat3::from([1,2,3,
                                         4,5,6,
                                         7,8,9]);

        assert_eq!(mat.cofactor(0,0), -3);
        assert_eq!(mat.cofactor(0,1), 6);
        assert_eq!(mat.cofactor(0,2), -3);
    }

    #[test]
    fn test_determinant()
    {
        assert_eq!(Mat3::from([1,0,0,0,1,0,0,0,1]).det(), 1);
        assert_eq!(Mat3::from([1,1,1,0,0,0,0,0,0]).det(), 0);
        assert_eq!(Mat3::from([1,1,1,0,1,1,0,2,1]).det(), -1);
    }

    #[test]
    fn test_inverse()
    {
        assert_eq!(Mat3::from([1,0,0,0,1,0,0,0,1]).inverse(), Some(Mat3::from([1,0,0,0,1,0,0,0,1])));
        assert_eq!(Mat3::from([1,0,0,0,0,0,0,0,0]).inverse(), None);
        assert_eq!(Mat3::from([1,0,1,0,1,0,0,0,1]).inverse(), Some(Mat3::from([1,0,-1,0,1,0,0,0,1])));
    }
}
