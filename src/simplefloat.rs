use std::ops::{Add, Sub, Mul, Div};
use std::cmp::{Ord, Ordering};

use crate::checked_math::{CheckedAdd, CheckedSub};

// Representation of negative partials are a little different here.
// Ex: -1/100 will actually be represented as (-1 + 99/100)

#[derive(Debug, Clone, Copy)]
pub struct SimpleFloat<T>
{
    pub integer: T,
    pub partial: T,
    pub max_partial: T
}

impl<T> PartialOrd for SimpleFloat<T>
where T: PartialOrd + Eq
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering>
    {
        if self.max_partial != other.max_partial
        {
            return None;
        }

        match self.integer.partial_cmp(&other.integer)
        {
            Some(Ordering::Equal) => self.partial.partial_cmp(&other.partial),
            x => x
        }
    }
}

impl<T> SimpleFloat<T>
where T: Copy + Mul<Output = T> + Div<Output = T>
{
    pub fn new(integer: T, partial: T, max_partial: T) -> Self
    {
        Self
        {
            integer: integer,
            partial: partial,
            max_partial: max_partial
        }
    }

    pub fn into_max_partial(self, new_max_partial: T) -> Self
    {
        Self
        {
            integer: self.integer,
            partial: self.partial * new_max_partial / self.max_partial,
            max_partial: new_max_partial
        }
    }
}


impl<T> PartialEq for SimpleFloat<T>
where T: Eq
{
    fn eq(&self, other: &Self) -> bool
    {
        self.max_partial == other.max_partial && self.integer == other.integer && self.partial == other.partial
    }
}

impl<T> Add<SimpleFloat<T>> for SimpleFloat<T>
where SimpleFloat<T>: CheckedAdd
{
    type Output = SimpleFloat<T>;

    fn add(self, other: SimpleFloat<T>) -> Self::Output
    {
        self.checked_add(other).unwrap()
    }
}

impl<T> Sub<SimpleFloat<T>> for SimpleFloat<T>
where SimpleFloat<T>: CheckedSub
{
    type Output = SimpleFloat<T>;

    fn sub(self, other: SimpleFloat<T>) -> Self::Output
    {
        self.checked_sub(other).unwrap()
    }
}

impl<T> CheckedAdd for SimpleFloat<T>
where T: Copy + Eq + Add<Output = T> + Sub<Output = T> + Ord + From<u8>
{
    fn checked_add(self, other: Self) -> Option<Self>
    {
        if self.max_partial != other.max_partial
        {
            return None;
        }

        Some(match self.partial
             {
                 x if self.max_partial <= other.partial + x => Self
                 {
                     max_partial: self.max_partial,
                     integer: self.integer + other.integer + T::from(1),
                     partial: self.partial - (self.max_partial - other.partial)
                 },
                 x if T::from(0) > other.partial + x => Self
                 {
                     max_partial: self.max_partial,
                     integer: self.integer + other.integer - T::from(1),
                     partial: other.partial + self.partial + self.max_partial
                 },
                 _ => Self
                 {
                     max_partial: self.max_partial,
                     integer: self.integer + other.integer,
                     partial: self.partial + other.partial
                 }
             }
        )
    }
}


impl<T> CheckedSub for SimpleFloat<T>
where T: Copy + Eq + Add<Output = T> + Sub<Output = T> + Ord + From<u8>
{
    fn checked_sub(self, other: Self) -> Option<Self>
    {
        if self.max_partial != other.max_partial
        {
            return None;
        }

        Some(match self.partial
             {
                 x if x >= other.partial => Self
                 {
                     max_partial: self.max_partial,
                     integer: self.integer - other.integer,
                     partial: self.partial - other.partial
                 },
                 _ => Self
                 {
                     max_partial: self.max_partial,
                     integer: self.integer - other.integer - T::from(1),
                     partial: self.partial - other.partial + self.max_partial
                 }
             }
        )
    }
}


impl<T> From<SimpleFloat<T>> for f32
where T: Into<f32>
{
    fn from(value: SimpleFloat<T>) -> Self
    {
        return value.integer.into() + (value.partial.into() / value.max_partial.into());
    }
}


impl<T> From<SimpleFloat<T>> for f64
where T: Into<f64>
{
    fn from(value: SimpleFloat<T>) -> Self
    {
        return value.integer.into() + (value.partial.into() / value.max_partial.into());
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_integer_initializes()
    {
        let a : SimpleFloat<u8> = SimpleFloat::new(1,2,10);

        assert!(a.integer == 1);
    }

    #[test]
    fn test_partial_initializes()
    {
        let a : SimpleFloat<u8> = SimpleFloat::new(1,2,10);

        assert!(a.partial == 2);
    }

    #[test]
    fn test_max_partial_initializes()
    {
        let a : SimpleFloat<u8> = SimpleFloat::new(1,2,10);

        assert!(a.max_partial == 10);
    }

    #[test]
    fn test_equality()
    {
        assert!(SimpleFloat::new(1,2,10) == SimpleFloat::new(1,2,10));
    }

    #[test]
    fn test_reflexive_from()
    {
        assert_eq!(SimpleFloat::from(SimpleFloat::new(1,9,10)), SimpleFloat::new(1,9,10));
    }

    #[test]
    fn test_inequality()
    {
        assert!(SimpleFloat::new(1,2,10) != SimpleFloat::new(2,2,10));
        assert!(SimpleFloat::new(1,2,10) != SimpleFloat::new(1,3,10));
        assert!(SimpleFloat::new(1,2,10) != SimpleFloat::new(1,2,11));
    }

    #[test]
    fn test_unchecked_addition()
    {
        assert_eq!(SimpleFloat::new(1,0,10) + SimpleFloat::new(0,1,10), SimpleFloat::new(1,1,10));
        assert_eq!(SimpleFloat::new(1,2,10) + SimpleFloat::new(0,1,10), SimpleFloat::new(1,3,10));
        assert_eq!(SimpleFloat::new(1,9,10) + SimpleFloat::new(0,1,10), SimpleFloat::new(2,0,10));
    }

    #[test]
    fn test_unchecked_substraction()
    {
        assert_eq!(SimpleFloat::new(1,2,10) - SimpleFloat::new(0,2,10), SimpleFloat::new(1,0,10));
        assert_eq!(SimpleFloat::new(1,2,10) - SimpleFloat::new(0,3,10), SimpleFloat::new(0,9,10));
    }

    #[test]
    fn test_simple_addition()
    {
        assert_eq!(SimpleFloat::new(1,0,10).checked_add(SimpleFloat::new(0,1,10)), Some(SimpleFloat::new(1,1,10)));
        assert_eq!(SimpleFloat::new(1,2,10).checked_add(SimpleFloat::new(0,1,10)), Some(SimpleFloat::new(1,3,10)));
    }

    #[test]
    fn test_invalid_addition()
    {
        assert!(SimpleFloat::new(1,0,10).checked_add(SimpleFloat::new(0,1,5)) == None);
    }

    #[test]
    fn test_overflow_addition()
    {
        assert_eq!(SimpleFloat::new(1,9,10).checked_add(SimpleFloat::new(0,1,10)), Some(SimpleFloat::new(2,0,10)));
    }

    #[test]
    fn test_simple_subtraction()
    {
        assert!(SimpleFloat::new(1,2,10).checked_sub(SimpleFloat::new(0,2,10)) == Some(SimpleFloat::new(1,0,10)));
    }

    #[test]
    fn test_underflow_subtraction()
    {
        assert_eq!(SimpleFloat::new(1,2,10).checked_sub(SimpleFloat::new(0,3,10)), Some(SimpleFloat::new(0,9,10)));
    }

    #[test]
    fn test_invalid_subtraction()
    {
        assert!(SimpleFloat::new(1,0,10).checked_sub(SimpleFloat::new(0,1,5)) == None);
    }

    #[test]
    fn test_into_f32()
    {
        assert_eq!(f32::from(SimpleFloat::<u8>::new(1,2,10)), 1.2 as f32);
        assert_eq!(f32::from(SimpleFloat::<u16>::new(1,2,10)), 1.2 as f32);
    }

    #[test]
    fn test_into_f64()
    {
        assert_eq!(f64::from(SimpleFloat::<u8>::new(1,2,10)), 1.2 as f64);
        assert_eq!(f64::from(SimpleFloat::<u16>::new(1,2,10)), 1.2 as f64);
        assert_eq!(f64::from(SimpleFloat::<u32>::new(1,2,10)), 1.2 as f64);
    }

    #[test]
    fn test_into_f32_in_a_somewhat_extreme_case()
    {
        assert_eq!(f64::from(SimpleFloat::<u16>::new(1,u16::max_value()-1,u16::max_value())), 1.9999847409781033 as f64);
    }

    #[test]
    fn test_convert_between_max_partials()
    {
        assert_eq!(SimpleFloat::new(1,9,10).into_max_partial(100), SimpleFloat::new(1,90,100));
        assert_eq!(SimpleFloat::new(1,8,10).into_max_partial(100), SimpleFloat::new(1,80,100));
        assert_eq!(SimpleFloat::new(1,11,100).into_max_partial(10), SimpleFloat::new(1,1,10));
    }
}
