use super::Mat4;

#[derive(Debug, PartialEq)]
pub enum FrustumDef
{
    FiniteDef
    {
        near: f32,
        far: f32,
        fov_x: f32,
        fov_y: f32
    },
    InfiniteDef
    {
        near: f32,
        fov_x: f32,
        fov_y: f32
    }
}


impl FrustumDef
{
    pub fn finite_from_params(near: f32, far: f32, fov_x: f32, fov_y: f32) -> Self
    {
        FrustumDef::FiniteDef
        {
            near: near,
            far: far,
            fov_x: fov_x,
            fov_y: fov_y
        }
    }

    pub fn infinite_from_params(near: f32, fov_x: f32, fov_y: f32) -> Self
    {
        FrustumDef::InfiniteDef
        {
            near: near,
            fov_x: fov_x,
            fov_y: fov_y
        }
    }
}


pub fn get_frustum_matrix(def: FrustumDef) -> Mat4<f32>
{
    match def
    {
        FrustumDef::FiniteDef
        {
            near,
            far,
            fov_x,
            fov_y
        } => {
            Mat4::from([[1.0/((fov_x/2.0).tan()), 0.0, 0.0, 0.0],
                        [0.0, 1.0/((fov_y/2.0).tan()), 0.0, 0.0],
                        [0.0, 0.0, -(far + near)/(far - near), -2.0*near*far/(far-near)],
                        [0.0, 0.0, -1.0, 0.0]])
        },

        FrustumDef::InfiniteDef
        {
            near,
            fov_x,
            fov_y
        } => {
            Mat4::from([[near, 0.0, fov_x, fov_y],
                        [0.0, 0.0, 0.0, 0.0],
                        [0.0, 0.0, 0.0, 0.0],
                        [0.0, 0.0, 0.0, 0.0]])
        }
    }
}


#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn test_finite_frustum_def()
    {
        assert_eq!(FrustumDef::finite_from_params(1.0, 50.0, 45.0, 45.0),
                   FrustumDef::FiniteDef
                   {
                       near: 1.0,
                       far: 50.0,
                       fov_x: 45.0,
                       fov_y: 45.0
                   }
        );
    }

    #[test]
    fn test_infinite_frustum_def()
    {
        assert_eq!(FrustumDef::infinite_from_params(1.0, 45.0, 45.0),
                   FrustumDef::InfiniteDef
                   {
                       near: 1.0,
                       fov_x: 45.0,
                       fov_y: 45.0
                   }
        );
    }

    #[test]
    fn test_get_finit_frustum()
    {
        assert_eq!(get_frustum_matrix(FrustumDef::FiniteDef
                                      {
                                          near: 1.0,
                                          far: 20.0,
                                          fov_x: (60.0_f32).to_radians(),
                                          fov_y: (60.0_f32).to_radians()
                                      }),
                   Mat4::<f32>::from([[1.7320507, 0.0, 0.0, 0.0],
                                      [0.0, 1.7320507, 0.0, 0.0],
                                      [0.0, 0.0, -1.1052631, -2.1052632],
                                      [0.0, 0.0, -1.0, 0.0]]));
    }
}
