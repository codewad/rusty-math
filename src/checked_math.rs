pub trait CheckedAdd : Sized
{
    fn checked_add(self, other: Self) -> Option<Self>;
}


pub trait CheckedSub : Sized
{
    fn checked_sub(self, other: Self) -> Option<Self>;
}
