use std::vec::Vec;
use std::ops::{Add, Mul};


#[derive(Debug, PartialEq)]
pub struct Matrix<T>
{
    dimensions: [usize; 2],
    values: Vec<T>
}


impl<T> Matrix<T>
where T: Copy
{
    pub fn from_array(dimensions: &[usize; 2], values: &[T]) -> Self
    {
        Self
        {
            dimensions: dimensions.clone(),
            values: Vec::from(values)
        }
    }

    pub fn transpose(self) -> Self
    {
        let columns: Vec<&[T]> = self.values.chunks(self.dimensions[1]).collect();
        let mut new_values: Vec<T> = Vec::default();

        for j in 0..self.dimensions[1]
        {
            for i in 0..self.dimensions[0]
            {
                new_values.push(columns[i][j]);
            }
        }

        Self
        {
            dimensions: [self.dimensions[1], self.dimensions[0]],
            values: new_values
        }
    }
}


impl<T> Mul for Matrix<T>
where T: Copy + Add<Output = T> + Mul<Output = T> + Default
{
    type Output = Matrix<T>;

    fn mul(self, other: Self) -> Self
    {
        if self.dimensions[1] != other.dimensions[0]
        {
            panic!("Mismatch dimensions in matrix multiplication");
        }

        let self_rows: Vec<&[T]> = self.values.chunks(self.dimensions[1]).collect();
        let other_rows: Vec<&[T]> = other.values.chunks(other.dimensions[1]).collect();
        let mut new_values: Vec<T> = Vec::new();

        for i in 0..self.dimensions[0]
        {
            for j in 0..other.dimensions[1]
            {
                let mut result: T = T::default();

                for k in 0..self.dimensions[1]
                {
                    result = result + (self_rows[i][k] * other_rows[k][j]);
                }

                new_values.push(result);
            }
        }

        Self
        {
            dimensions: [self.dimensions[0], other.dimensions[1]],
            values: new_values
        }
    }
}


#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_from_array()
    {
        let _matrix: Matrix<u8> = Matrix::from_array(&[2, 2], &[1, 2, 3, 4]);
    }

    #[test]
    fn test_equality()
    {
        assert_eq!(Matrix::from_array(&[2, 2], &[1, 2, 3, 4]), Matrix::from_array(&[2, 2], &[1, 2, 3, 4]));
    }

    #[test]
    fn test_inequality()
    {
        assert_ne!(Matrix::from_array(&[2, 2], &[2, 2, 3, 4]), Matrix::from_array(&[2, 2], &[1, 2, 3, 4]));
        assert_ne!(Matrix::from_array(&[1, 2], &[2, 2]), Matrix::from_array(&[2, 2], &[1, 2, 3, 4]));
        assert_ne!(Matrix::from_array(&[2, 2], &[2, 2, 3, 4]), Matrix::from_array(&[1, 2], &[1, 2]));
    }

    #[test]
    fn test_multiplication_1x1_x_1x1()
    {
        assert_eq!(
            Matrix::from_array(&[1, 1], &[1]) * Matrix::from_array(&[1, 1], &[1]),
            Matrix::from_array(&[1, 1], &[1])
        );
        assert_eq!(
            Matrix::from_array(&[1, 1], &[2]) * Matrix::from_array(&[1, 1], &[3]),
            Matrix::from_array(&[1, 1], &[6])
        );
    }

    #[test]
    fn test_multiplication_1x1_x_1x2()
    {
        assert_eq!(
            Matrix::from_array(&[1, 1], &[1]) * Matrix::from_array(&[1, 2], &[1, 2]),
            Matrix::from_array(&[1, 2], &[1, 2])
        );
        assert_eq!(
            Matrix::from_array(&[1, 1], &[2]) * Matrix::from_array(&[1, 2], &[1, 2]),
            Matrix::from_array(&[1, 2], &[2, 4])
        );
    }

    #[test]
    fn test_multiplication_2x1_x_1x1()
    {
        assert_eq!(
            Matrix::from_array(&[2, 1], &[1, 2]) * Matrix::from_array(&[1, 1], &[1]),
            Matrix::from_array(&[2, 1], &[1, 2])
        );
        assert_eq!(
            Matrix::from_array(&[2, 1], &[1, 2]) * Matrix::from_array(&[1, 1], &[2]),
            Matrix::from_array(&[2, 1], &[2, 4])
        );
    }

    #[test]
    fn test_multiplication_2x1_x_1x2()
    {
        assert_eq!(
            Matrix::from_array(&[2, 1], &[1, 2]) * Matrix::from_array(&[1, 2], &[1, 2]),
            Matrix::from_array(&[2, 2], &[1, 2, 2, 4])
        );
        assert_eq!(
            Matrix::from_array(&[2, 1], &[2, 1]) * Matrix::from_array(&[1, 2], &[1, 2]),
            Matrix::from_array(&[2, 2], &[2, 4, 1, 2])
        );
    }

    #[test]
    fn test_multiplication_2x2_x_2x2()
    {
        assert_eq!(
            Matrix::from_array(&[2, 2], &[1, 1, 1, 1]) * Matrix::from_array(&[2, 2], &[1, 1, 1, 1]),
            Matrix::from_array(&[2, 2], &[2, 2, 2, 2])
        );

        assert_eq!(
            Matrix::from_array(&[2, 2], &[1, 2, 2, 1]) * Matrix::from_array(&[2, 2], &[2, 1, 1, 2]),
            Matrix::from_array(&[2, 2], &[4, 5, 5, 4])
        );
    }

    #[test]
    fn test_multiplication_identity()
    {
        assert_eq!(
            Matrix::from_array(&[2, 2], &[1, 0, 0, 1]) * Matrix::from_array(&[2, 2], &[1, 2, 3, 4]),
            Matrix::from_array(&[2, 2], &[1, 2, 3, 4])
        );
        assert_eq!(
            Matrix::from_array(&[2, 2], &[1, 2, 3, 4]) * Matrix::from_array(&[2, 2], &[1, 0, 0, 1]),
            Matrix::from_array(&[2, 2], &[1, 2, 3, 4])
        );
    }

    #[test]
    fn test_transpose()
    {
        assert_eq!(Matrix::from_array(&[2, 2], &[1, 0, 0, 1]).transpose(), Matrix::from_array(&[2, 2], &[1, 0, 0, 1]));
        assert_eq!(Matrix::from_array(&[2, 2], &[1, 1, 0, 1]).transpose(), Matrix::from_array(&[2, 2], &[1, 0, 1, 1]));
        assert_eq!(Matrix::from_array(&[2, 2], &[4, 2, 3, 1]).transpose(), Matrix::from_array(&[2, 2], &[4, 3, 2, 1]));
        assert_eq!(Matrix::from_array(&[2, 3], &[1, 2, 3, 4, 5, 6]).transpose(),
                   Matrix::from_array(&[3, 2], &[1, 4, 2, 5, 3, 6]));
    }
}
